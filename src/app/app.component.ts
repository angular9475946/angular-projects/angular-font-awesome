import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  faAddressBook,
  faAddressCard,
  faBell,
  faBellSlash,
  faBookmark,
  faBuilding,
  faCalendar,
  faCalendarCheck,
  faCalendarDays,
  faCalendarMinus,
  faCalendarPlus,
  faCalendarXmark,
  faChartBar,
  faChessBishop,
  faChessKing,
  faChessKnight,
  faChessPawn,
  faChessQueen,
  faChessRook,
  faCircleCheck,
  faCircleDot,
  faCircleDown,
  faCircleLeft,
  faCirclePause,
  faCirclePlay,
  faCircleQuestion,
  faCircleRight,
  faCircleStop,
  faCircleUp,
  faCircleUser
} from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, FontAwesomeModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'angular-fonts-awesome';
  protected readonly faAddressBook = faAddressBook;
  protected readonly faAddressCard = faAddressCard;
  protected readonly faBellSlash = faBellSlash;
  protected readonly faBell = faBell;
  protected readonly faBookmark = faBookmark;
  protected readonly faBuilding = faBuilding;
  protected readonly faCalendar = faCalendar;
  protected readonly faCalendarCheck = faCalendarCheck;
  protected readonly faCalendarDays = faCalendarDays;
  protected readonly faCalendarMinus = faCalendarMinus;
  protected readonly faCalendarPlus = faCalendarPlus;
  protected readonly faCalendarXmark = faCalendarXmark;
  protected readonly faChartBar = faChartBar;
  protected readonly faChessBishop = faChessBishop;
  protected readonly faChessKing = faChessKing;
  protected readonly faChessKnight = faChessKnight;
  protected readonly faChessPawn = faChessPawn;
  protected readonly faChessQueen = faChessQueen;
  protected readonly faChessRook = faChessRook;
  protected readonly faCircleCheck = faCircleCheck;
  protected readonly faCircleDot = faCircleDot;
  protected readonly faCircleDown = faCircleDown;
  protected readonly faCircleLeft = faCircleLeft;
  protected readonly faCirclePause = faCirclePause;
  protected readonly faCirclePlay = faCirclePlay;
  protected readonly faCircleQuestion = faCircleQuestion;
  protected readonly faCircleRight = faCircleRight;
  protected readonly faCircleStop = faCircleStop;
  protected readonly faCircleUp = faCircleUp;
  protected readonly faCircleUser = faCircleUser;
}
